set -e

directory_path=$HOME/Projects/automate-keep-alive-testing/test-results/socket-reconnection
mkdir $directory_path

echo "Testing socket reconnection experiment" > $directory_path/Readme.md

echo "Starting Experiment........."

# sending request to the /keep/alive/off
echo "GET http://localhost:1337/keep/alive/off" | vegeta attack -duration=600s -rate=70  -output=$directory_path/phase1.bin
vegeta report -type=json $directory_path/phase1.bin > $directory_path/off.json
vegeta report -type=hdrplot $directory_path/phase1.bin > $directory_path/off-plot
rm $directory_path/phase1.bin
curl http://localhost:1337/sendfile?file=off.csv > $directory_path/off.csv

# sleep for 2 minute
sleep 120

# sending requests to the /keep/alive/on route with keep-alive socket timeout to be 300s
echo "GET http://localhost:1337/keep/alive/on?timeout=300" | vegeta attack -duration=600s -rate=70 -output=$directory_path/phase1.bin
vegeta report -type=json $directory_path/phase1.bin > $directory_path/on.json
vegeta report -type=hdrplot $directory_path/phase1.bin > $directory_path/on-plot
rm $directory_path/phase1.bin
curl http://localhost:1337/sendfile?file=on.csv > $directory_path/on.csv

# analyse
$root_directory/lib/average.js --off=$directory_path/off.csv --on=$directory_path/on.csv --period=2s --output=$directory_path/result.csv
$root_directory/lib/plot.py $directory_path/result.csv $directory_path

echo "Ends Experiment........."