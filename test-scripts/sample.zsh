set -e
root_directory=$HOME/Projects/automate-keep-alive-testing
directory_path=$HOME/Projects/automate-keep-alive-testing/test-results/sample
echo $directory_path
mkdir $directory_path

echo "sample test" > $directory_path/Readme.md

# off
echo "GET http://localhost:1337/keep/alive/off" | vegeta attack -duration=30s -rate=40  -output=$directory_path/phase1.bin
vegeta report -type=json $directory_path/phase1.bin > $directory_path/off.json
vegeta report -type=hdrplot $directory_path/phase1.bin > $directory_path/off-plot
rm $directory_path/phase1.bin
curl http://localhost:1337/sendfile?file=off.csv > $directory_path/off.csv
# sleep 2 seconds
sleep 30

# on
echo "GET http://localhost:1337/keep/alive/on?timeout=10" | vegeta attack -duration=30s -rate=40 -output=$directory_path/phase1.bin
vegeta report -type=json $directory_path/phase1.bin > $directory_path/on.json
vegeta report -type=hdrplot $directory_path/phase1.bin > $directory_path/on-plot
rm $directory_path/phase1.bin
curl http://localhost:1337/sendfile?file=on.csv > $directory_path/on.csv

# analyse
$root_directory/lib/average.js --off=$directory_path/off.csv --on=$directory_path/on.csv --period=2s --output=$directory_path/result.csv
$root_directory/lib/plot.py $directory_path/result.csv $directory_path