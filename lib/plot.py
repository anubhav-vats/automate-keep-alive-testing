#!/usr/bin/python
import numpy as np
from matplotlib import pyplot as plt
import sys

fname = sys.argv[1]
output_directory = sys.argv[2]

data = np.genfromtxt(fname, delimiter=',', names=["x", "tcpAvgOff", "dnsAvgOff", "totalAvgOff", "dnsAvgOn", "totalAvgOn", "tcpAvgOn" ])
data = np.delete(data, 0)


# TCP off vs on
plt.figure(1)
plt.plot(data['x'], data['tcpAvgOff'], 'r-', label='Average TCP Connection keep-alive is disabled')
plt.plot(data['x'], data['tcpAvgOn'], 'b-', label='Average TCP Connection keep-alive is enabled')
plt.legend()
plt.savefig(output_directory + '/tcp.png')

# DNS off vs on
plt.figure(2)
plt.plot(data['x'], data['dnsAvgOff'], 'r-', label='Average DNS lookup keep-alive is disabled')
plt.plot(data['x'], data['dnsAvgOn'], 'b-', label='Average DNS lookup keep-alive is enabled')
plt.legend()
plt.savefig(output_directory + '/dns.png')


# Total off vs on
plt.figure(3)
plt.plot(data['x'], data['totalAvgOff'], 'r-', label='Average HTTP round-trip keep-alive is disabled')
plt.plot(data['x'], data['totalAvgOn'], 'b-', label='Average HTTP round-trip keep-alive is enabled')
plt.legend()
plt.savefig(output_directory + '/roundtrip.png')
