#!/Users/postman/.nvm/versions/node/v15.6.0/bin/node

/**
 * Description This program must provide functionality
 * average --off=<path-to-off.csv>  --on=<path-to-on.csv> --period=<either in seconds or min>
 */
const args = require('yargs/yargs')(process.argv.slice(2)).argv,
    csvParser = require('csv-parser'),
    createCsvWriter = require('csv-writer').createObjectCsvWriter,
    path = require('path'),
    fs = require('fs');


function isValidRow(rowData) {
    if (isNaN(rowData.tcp) || isNaN(rowData.dns) || isNaN(rowData.total) || isNaN(rowData.ts)) {
        return false;
    }
    return true;
}

const off = { rows: [], isRead: false, map: {} }, on = { rows: [], isRead: false, map: {} };

function read(file) {
    fs.createReadStream(args[file])
        .pipe(csvParser())
        .on('data', function (row) {
            const rowData = {
                tcp: parseFloat(row.tcp),
                dns: parseFloat(row.dns),
                total: parseFloat(row.total),
                ts: parseInt(row.timeStart, 10)
            }
            if (isValidRow(rowData)) {
                file === 'off' ? off.rows.push(rowData) : on.rows.push(rowData);
            }
        })
        .on('end', function () {
            file === 'off' ? off.isRead = true : on.isRead = true;
            if (off.isRead && on.isRead) {
                const calc = average();
                off.map = calc(off.rows);
                on.map = calc(on.rows);
                // console.log(off);
                // console.log(on)
                write();
            }
        });
};

function average() {
    const period = {};
    console.log(args.period);
    period.type = args.period[args.period.length -1];
    console.log(period.type);
    if (period.type !== 'm' && period.type !== 's') {
        throw new TypeError('Period should either be in minutes or seconds');
    }

    period.value = parseInt(args.period.substring(0, args.period.length - 1), 10);

    function getPeriodValue() {
        return period.type === 'm' ? period.value * 1000 * 60 : period.value * 1000;
    }

    return function calculate(arr) {
        arr.sort(function(a, b) {
            return a.ts - b.ts
        });
        let tcpSum = 0, dnsSum = 0, count = 0, startTs = null, totalSum = 0, parts = 0, map = {};
        arr.forEach(function (val) {
            if (!startTs) {
                startTs = val.ts;
            }
            if (val.ts <= startTs + getPeriodValue()) {
                dnsSum += val.dns;
                tcpSum += val.tcp;
                totalSum += val.total;
                count++;
            }
            else {
                map[parts++] = {
                    dnsAvg: dnsSum / count,
                    tcpAvg: tcpSum / count,
                    totalAvg: totalSum / count,
                    period: parts * period.value
                };
                dnsSum = 0;
                tcpSum = 0;
                count = 0;
                totalSum = 0;
                startTs = val.ts;
            }
        });

        if (count !== 0) {
            map[parts++] = {
                dnsAvg: dnsSum / count,
                tcpAvg: tcpSum / count,
                totalAvg: totalSum / count,
                period: parts * period.value
            };
            dnsSum = 0;
            tcpSum = 0;
            count = 0;
            totalSum = 0;
        }
        return map;
    }
}

function write() {
    const records = []
    const csvWriter = createCsvWriter({
        path: args.output,
        header: [
            { id: 'period', title: 'period' },
            { id: 'tcpAvgOff', title: 'Avg. TCP connection time keep-alive off' },
            { id: 'dnsAvgOff', title: 'Avg. DNS lookup time keep-alive off' },
            { id: 'totalAvgOff', title: 'Avg. Total time when keep-alive off' },
            { id: 'dnsAvgOn', title: 'Avg. DNS lookup time when keep-alive on' },
            { id: 'totalAvgOn', title: 'Avg. Total time when keep-alive on' },
            { id: 'tcpAvgOn', title: 'Avg. TCP connection when keep-alive on' }
        ]     
    });
    Object.keys(off.map).forEach( function (key) {
        records.push({
            tcpAvgOff: off.map[key].tcpAvg,
            dnsAvgOff: off.map[key].dnsAvg,
            totalAvgOff: off.map[key].totalAvg,
            tcpAvgOn: on.map[key].tcpAvg,
            dnsAvgOn: on.map[key].dnsAvg,
            totalAvgOn: on.map[key].totalAvg,
            period: off.map[key].period
        });
    });
    csvWriter.writeRecords(records)
        .then(()=> {
            console.log('Completed successully please check file', args.output);
        })
        .catch((err) => {
            console.log(err);
        });
}

read('off');
read('on')